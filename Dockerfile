FROM nvidia/cuda:8.0-cudnn5-runtime-ubuntu16.04

MAINTAINER vsv10000ego@gmail.com

ENV CPU_CORE 1

RUN \
	apt-get update -qq && \
    apt-get install -y \
    git bzip2 wget \
    g++ make python python3 \
    zlib1g-dev automake autoconf libtool subversion \
    libatlas-base-dev sox flac gawk swig python-dev python-setuptools curl unzip

WORKDIR /usr/local/

COPY $PWD/VOLUME_DATA /usr/local/VOLUME_DATA

COPY $PWD/VOLUME_DATA/scripts/deployKaldi.sh deployKaldi.sh

RUN chmod +x deployKaldi.sh

RUN git clone https://github.com/kaldi-asr/kaldi.git