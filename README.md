Данный рецепт служит для построения docker образа распознавания речи основанном на рецептах kaldi.

##Примичания
Используемые рецепты kaldi взяты из https://github.com/freerussianasr/recipes.git и https://github.com/grib0ed0v/kaldi-for-russian

Для простоты скачивания в сборку добавлен скрипт на perl для скачивания с google диска. Для скачивания файлы должны быть расшарены для всего интернета.

Сам скрипт - /dockerspeechrecognizer/VOLUME_DATA/scripts/gdown.pl

Использование: gdown.pl <ссылка на скачивания с google диска> <имя для сохранения>

##Обновление внещних зависимостей и скриптов.

Для простоты развёртывания на имеющихся архитектурах в репозиторий сохраненно содержимое несколько внешних зависимостей:

1) В папке /dockerspeechrecognizer/VOLUME_DATA/localLibs нужно заменить файл srilm.tgz. Свежая версия доступна - http://www.speech.sri.com/projects/srilm/download.html

2) В папке dockerspeechrecognizer/VOLUME_DATA/kaldi_recipes нужно обновить содержимое voxforge_ru/s5. Она содержит файлы из voxforge_ru папки репозитория https://github.com/freerussianasr/recipes.git. 

3) Скрипт для скачивания с google диска /dockerspeechrecognizer/VOLUME_DATA/scripts/gdown.pl и доступен https://github.com/circulosmeos/gdown.pl

##Развёртывание

docker build -t gva_recognizer_kaldi .

## Взять готовую Русскую сетку

В контенере

cd /usr/local/kaldi/egs/voxforge_ru/s5

./get_data.sh

cat dnn.tar.gz.* > dnn.tar.gz

tar -xvzf dnn.tar.gz

## Обучение Русскому

В контенере

cd /usr/local/kaldi/egs/voxforge_ru/s5

./get_data.sh

./run.sh
