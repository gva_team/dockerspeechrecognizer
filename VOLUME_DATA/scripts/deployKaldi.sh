#!/bin/bash

# Use the newest kaldi version
#git clone https://github.com/kaldi-asr/kaldi.git

cd /usr/local/kaldi/tools
extras/check_dependencies.sh
make -j $CPU_CORE

cd /usr/local/kaldi/src
./configure && make depend -j $CPU_CORE && make -j $CPU_CORE

# add ru recipe
mv /usr/local/VOLUME_DATA/kaldi_recipes/voxforge_ru/* /usr/local/kaldi/egs/voxforge_ru
# add srilm
mv /usr/local/VOLUME_DATA/localLibs/* /usr/local/kaldi/tools

# get lang corp data for ru kaldi

cd /usr/local/kaldi/egs/voxforge_ru/s5
ln -s ./../../voxforge/s5 steps
ln -s ./../../voxforge/s5 utils

# install srilm
cd /usr/local/kaldi/tools
./install_srilm.sh

cd /usr/local/kaldi/tools/extras
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install numpy

# g2p install
./install_sequitur.sh
./install_mpg123.sh

# ATLAS is need?

cd /usr/local/kaldi/egs/voxforge_ru/s5
pip install g2p
echo source /usr/local/kaldi/tools/env.sh >> path.sh

cp /usr/local/VOLUME_DATA/scripts/makeXToAllSH.sh ./makeXToAllSH.sh
chmod +x ./makeXToAllSH.sh
./makeXToAllSH.sh


cd /usr/local/kaldi/egs/voxforge_ru/s5
cp /usr/local/VOLUME_DATA/scripts/recognizeWav.sh ./recognizeWav.sh

./getdata.sh

./run.sh

#prepare tri3b model to recognize. If we use another model we mast fix recognize script
cp ./exp/tri3b/* .

cd /usr/local/
cp /usr/local/VOLUME_DATA/scripts/runRecogniseCircle.py