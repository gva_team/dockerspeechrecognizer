#!/bin/bash

DATA_DIR=./wavData

local/create-corpus.sh $DATA_DIR $@ >/dev/null 2>/dev/null

steps/make_mfcc.sh --nj 1 $DATA_DIR exp/make_mfcc mfcc >/dev/null 2>/dev/null

steps/compute_cmvn_stats.sh $DATA_DIR exp/make_mfcc mfcc >/dev/null 2>/dev/null

steps/decode.sh --config ./conf/decode.config --nj 1 --model ./exp/tri3b/final.mdl ./exp/tri3b/graph/ $DATA_DIR ./decodResult >/dev/null 2>/dev/null

grep $@ ./decodResult/log/decode.1.log | grep -v "LOG" | grep -i ".wav" | grep -v "info" | grep -v "//"

return 0