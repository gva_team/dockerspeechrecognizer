import os
import time
from shutil import copyfile
import subprocess

CICLE_TIME = 1000

recogniserPath = "/usr/local/kaldi/egs/voxforge_ru/s5"
tasksFile = "/usr/local/kaldi/egs/voxforge_ru/Task"
lastResult = "/usr/local/kaldi/egs/voxforge_ru/Result"
wordsFile = recogniserPath + "/exp/tri3b/graph/words.txt"

def copyWords(wordsFileName):
    os.remove(tasksFile)
    copyfile(wordsFileName, wordsFile)
    pass

def recognize(soundFileName, wordsFileName):
    copyWords(wordsFileName)
    cmd = "." + recogniserPath + "/recognizeWav.sh"
    recProc = subprocess.Popen([cmd, soundFileName], stdout=subprocess.PIPE)
    recProc.wait()
    recRez = str(recProc.communicate()[0])
    print("Recognized: '" + recRez + "'")
    return recRez

def currentTask():
    if os.path.isfile(tasksFile):
        soundFileName, wordsFileName = "/usr/local/kaldi/egs/voxforge_ru/test2.wav", "/usr/local/kaldi/egs/voxforge_ru/words.txt"
        return soundFileName, wordsFileName
    return []

def removeTask():
    os.remove(tasksFile)
    return

def removeResult():
    os.remove(lastResult)
    return

def clearState():
    removeTask()
    removeResult()
    return

def makeResultFile(result):
    resultFile = open(lastResult, 'w')
    resultFile.write(result)
    return

def waitCicle():
    time.sleep(CICLE_TIME)
    return

def main():
    os.chdir(recogniserPath)
    clearState()
    while(True):
        task = currentTask()
        if len(task) > 0:
            result = recognize(*task)
            makeResultFile(result)
            removeTask()
        else:
            waitCicle()